Feature: Tasks
  As a user
  I want to test image search functionality
  So that I can be sure that it works correctly

  Scenario Outline: User navigates to the Google search page, searches for an image, and verifies that an image tab contains images
    Given User opens '<homePage>' and searches for an image
    When User clicks on imageButton
    Then User verifies that an image tab contains images

    Examples:
    |homePage               |
    |https://www.google.com/|