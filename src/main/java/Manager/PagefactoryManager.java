package Manager;
import org.openqa.selenium.WebDriver;
import Pages.*;
public class PagefactoryManager {
    WebDriver driver;

    public PagefactoryManager(WebDriver driver){
        this.driver = driver;
    }

    public GooglePage getGooglePage(){
        return new GooglePage(driver);
    }

    public SearchPage getSearchPage(){
        return new SearchPage(driver);
    }
}
