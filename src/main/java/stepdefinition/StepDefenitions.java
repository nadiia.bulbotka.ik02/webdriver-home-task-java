package stepdefinition;

import Manager.PagefactoryManager;
import Pages.BasePage;
import Pages.GooglePage;
import Pages.SearchPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class StepDefenitions {
    PagefactoryManager pagefactoryManager;
    WebDriver driver;
    BasePage basePage;
    GooglePage googlePage;
    SearchPage searchPage;

    @Before
    public void testsSetUp(){
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pagefactoryManager = new PagefactoryManager(driver);
    }
    @Given("User opens {string} and searches for an image")
    public void openGooglePage(final String URL){
    googlePage = pagefactoryManager.getGooglePage();
    googlePage.openGooglePage(URL);
    googlePage.inputSearchWord("image");
    }

    @When("User clicks on imageButton")
    public void userClicksOnImageButton() {
        searchPage = pagefactoryManager.getSearchPage();
        searchPage.clickImagesButton();
    }

    @Then("User verifies that an image tab contains images")
    public void userVerifiesThatAnImageTabContainsImages() {
        Assert.assertTrue(searchPage.checkImagesExists());
    }
    @After
    public void tearDown(){
        driver.close();
    }
}
