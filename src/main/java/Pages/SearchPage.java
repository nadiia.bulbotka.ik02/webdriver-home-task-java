package Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchPage extends BasePage{
    @FindBy(xpath = "//a[@data-hveid = 'CAEQAw']")
    private WebElement imagesButton;

    @FindBy(xpath = "//div[@jsname = 'BN6WQc']//img")
    private List<WebElement> imagesField;

    public void clickImagesButton(){
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", imagesButton);
        waitForPageLoadComplete(WAIT_TIME);
    }
    public boolean checkImagesExists(){
        return imagesField.size()>0;
    }
    public SearchPage(WebDriver driver) {
        super(driver);
    }
}
