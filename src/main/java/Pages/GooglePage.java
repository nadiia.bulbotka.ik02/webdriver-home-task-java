package Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GooglePage extends BasePage{
    public void openGooglePage(final String url){
        driver.get(url);
    }
    @FindBy(xpath = "//input[@class='gLFyf gsfi']")
    private WebElement searchInput;

    public void inputSearchWord(final String searchWord){
    searchInput.sendKeys(searchWord, Keys.ENTER);
    waitForPageLoadComplete(WAIT_TIME);
    }
    public GooglePage(WebDriver driver){
        super(driver);
    }
}
